import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePageComponent } from '../components/home-page/home-page.component';
import { AboutPageComponent } from '../components/about-page/about-page.component';
import { SocialPostsComponent } from '../components/social-posts/social-posts.component'; 


const routes: Routes = [
  { path: '', component: HomePageComponent, },
  { path: 'about-page', component: AboutPageComponent, },
  { path: 'social-post', component: SocialPostsComponent, },  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }