import { Component } from '@angular/core';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons/faTwitterSquare';
import { faPinterest } from '@fortawesome/free-brands-svg-icons/faPinterest';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SeoService } from './seo.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular7-SocialPosts';

  	fbIcon = faFacebookSquare;
	pinIcon = faPinterest;
	tweetIcon = faTwitterSquare;
	whatIcon = faWhatsapp;
	public twitterData: any;
	name = '@ngx-share/buttons';
	imageurl = 'https://www.gettyimages.com/gi-resources/images/CreativeLandingPage/HP_Sept_24_2018/CR3_GettyImages-159018836.jpg';

		constructor(public fa: FontAwesomeModule, private seo: SeoService) {
		console.log('app component constructor called');
	}

	ngOnInit() {

		this.seo.generateTags({
			title: 'UnodeCero Sports',
			description: 'La aplicación que es la polla',
			image: 'https://www.decotrend.sk/uploads/images/product/Fototapeta_Futbal_18621.jpg.large.jpg',
			url: 'http://angularssr.ml/',			
		})
		console.log('app component generateTags called');
	}



}
