import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from'@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SocialPostsComponent } from 'src/components/social-posts/social-posts.component';
import { HomePageComponent } from 'src/components/home-page/home-page.component';
import { AboutPageComponent } from 'src/components/about-page/about-page.component';

import { HttpModule } from '@angular/http';

//botones RS
import { ShareButtonsModule } from 'ngx-sharebuttons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

//Necesario para la parte de SEO y compartir info en RS
import { SeoService } from './seo.service';



@NgModule({
  declarations: [
    AppComponent,
    SocialPostsComponent,
    HomePageComponent,
    AboutPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,  
    HttpClientModule,
    HttpModule,  
    FontAwesomeModule,
    ShareButtonsModule.forRoot()

  ],
  providers: [SeoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
