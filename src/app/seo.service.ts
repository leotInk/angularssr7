import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable()
export class SeoService {

    constructor(private meta: Meta) { }

    generateTags(config) {
        config = {
            title: 'Ha llegado la nueva aplicación definitiva para el deporte [DIMANICO]',
            description: 'La herramienta que revolucionará los clubes deportivos  - Te lo vas a perder? [DIMANICO]',
            image: 'https://www.decotrend.sk/uploads/images/product/Fototapeta_Futbal_18621.jpg.large.jpg',
            slug: '',
            url: 'http://angularssr.ml/${config.slug}',
            site: 'UnodeCero Sports',

        }

        this.meta.updateTag({ name: 'twitter:card', content: 'summary' });
        this.meta.updateTag({ name: 'twitter:site', content: config.site });
        this.meta.updateTag({ name: 'twitter:title', content: config.title });
        this.meta.updateTag({ name: 'twitter:description', content: config.description });
        this.meta.updateTag({ name: 'twitter:image', content: config.image });

        this.meta.updateTag({ property: 'og:type', content: 'article' });
        this.meta.updateTag({ property: 'og:site_name', content: config.site });
        this.meta.updateTag({ property: 'og:title', content: config.title });
        this.meta.updateTag({ property: 'og:description', content: config.description });
        this.meta.updateTag({ property: 'og:image', content: config.image });
        this.meta.updateTag({ property: 'og:url', content: config.url });
        
        // this.meta.addTag({ name: 'twitter:card', content: 'summary' });
        // this.meta.addTag({ name: 'twitter:site', content: config.site });
        // this.meta.addTag({ name: 'twitter:title', content: config.title });
        // this.meta.addTag({ name: 'twitter:description', content: config.description });
        // this.meta.addTag({ name: 'twitter:image', content: config.image });

        // this.meta.addTag({ property: 'og:type', content: 'article' });
        // this.meta.addTag({ property: 'og:site_name', content: config.site });
        // this.meta.addTag({ property: 'og:title', content: config.title });
        // this.meta.addTag({ property: 'og:description', content: config.description });
        // this.meta.addTag({ property: 'og:image', content: config.image });
        // this.meta.addTag({ property: 'og:url', content: config.url });
    }

}

