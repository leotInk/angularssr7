import { Component, OnInit } from '@angular/core';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons/faTwitterSquare';
import { faPinterest } from '@fortawesome/free-brands-svg-icons/faPinterest';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SeoService } from 'src/app/seo.service';

@Component({
  selector: 'about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.sass']
})
export class AboutPageComponent implements OnInit {

  constructor(public fa: FontAwesomeModule, private seo: SeoService) {
  console.log('about-page component constructor called');
   }

  fbIcon = faFacebookSquare;
  pinIcon = faPinterest;
  tweetIcon = faTwitterSquare;
  whatIcon = faWhatsapp;
  public twitterData: any;
  name = '@ngx-share/buttons';
  imageurl = 'https://www.gettyimages.com/gi-resources/images/CreativeLandingPage/HP_Sept_24_2018/CR3_GettyImages-159018836.jpg';

  ngOnInit() {
    this.seo.generateTags({
      title: 'About Page', 
      description: 'This is my about page - did I mention that its link bot friendly?', 
      image: 'https://instafire-app.firebaseapp.com/assets/dog.jpeg',
      slug: 'about-page'
    })
    console.log('about-page component generateTags called');
  }

}
