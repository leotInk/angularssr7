import { Component, OnInit } from '@angular/core';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons/faTwitterSquare';
import { faPinterest } from '@fortawesome/free-brands-svg-icons/faPinterest';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SeoService } from 'src/app/seo.service';


@Component({
  selector: 'home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.sass'],
})
export class HomePageComponent implements OnInit {

  constructor(public fa: FontAwesomeModule, private seo: SeoService) {
  console.log('home-page component constructor called');
   }

  fbIcon = faFacebookSquare;
  pinIcon = faPinterest;
  tweetIcon = faTwitterSquare;
  whatIcon = faWhatsapp;
  public twitterData: any;
  name = '@ngx-share/buttons';
  imageurl = 'https://www.gettyimages.com/gi-resources/images/CreativeLandingPage/HP_Sept_24_2018/CR3_GettyImages-159018836.jpg';

  ngOnInit() {
    this.seo.generateTags({
      title: 'Home Page', 
      description: 'My SEO friendly home page in Angular 5', 
      image: 'https://instafire-app.firebaseapp.com/assets/camel.jpeg'
    })
     console.log('home-page component generateTags called');
  }

}
