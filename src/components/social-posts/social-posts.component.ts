import { Component, OnInit, Input } from '@angular/core';
import { UserPost } from 'src/models/UserPost.model'
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons/faTwitterSquare';
import { faPinterest } from '@fortawesome/free-brands-svg-icons/faPinterest';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SeoService } from 'src/app/seo.service';

@Component({
  selector: 'app-social-posts',
  templateUrl: './social-posts.component.html',
  styleUrls: ['./social-posts.component.scss']
})
export class SocialPostsComponent implements OnInit {


  constructor(public fa: FontAwesomeModule, private seo: SeoService) {
  console.log('social-post component constructor called');
   }

  fbIcon = faFacebookSquare;
  pinIcon = faPinterest;
  tweetIcon = faTwitterSquare;
  whatIcon = faWhatsapp;
  public twitterData: any;
  name = '@ngx-share/buttons';
  imageurl = 'https://www.gettyimages.com/gi-resources/images/CreativeLandingPage/HP_Sept_24_2018/CR3_GettyImages-159018836.jpg';

  socialPostList: UserPost [] = [{postTitle: "Placeholder Post Title", 
  postText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", 
  postDate: new Date("2015-03-25"), 
  postVoteScore: 0 }];
  showPostFormBool: boolean = false;
  newPost : UserPost; 


  createPost(newPostTitle: string, newPostText : string){
    var currentDate = new Date();
    this.newPost = {
      postTitle: newPostTitle, 
      postText: newPostText, 
      postDate: currentDate, 
      postVoteScore: 0 };
    this.socialPostList.unshift(this.newPost);
    this.showPostFormBool = false;
  }


  showPostForm() {
      this.showPostFormBool = true;
  }

  hidePostForm(){
    this.showPostFormBool = false;

  }


  addToPostScore(i : number){
    this.socialPostList[i].postVoteScore = this.socialPostList[i].postVoteScore + 1;
  }

  subtractFromPostScore(i : number){
    this.socialPostList[i].postVoteScore = this.socialPostList[i].postVoteScore - 1;
  }

  ngOnInit() {
  this.seo.generateTags({
      title: 'UnodeCero Sports',
      description: 'La aplicación que es la polla',
      image: 'https://www.decotrend.sk/uploads/images/product/Fototapeta_Futbal_18621.jpg.large.jpg',
      url: 'http://angularssr.ml/'
    })
    console.log('social-post component generateTags called');
  }

}
